package net.pl3x.bukkit.spawn.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Config {
    public static String LANGUAGE_FILE;
    public static boolean USE_TELEPORT_SOUNDS;
    public static Location SPAWN;

    private Config() {
    }

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        USE_TELEPORT_SOUNDS = config.getBoolean("use-teleport-sounds", true);

        World world = Bukkit.getWorld(config.getString("spawn.world", ""));
        if (world != null) {
            SPAWN = new Location(world,
                    config.getDouble("spawn.x", 0),
                    config.getDouble("spawn.y", 0),
                    config.getDouble("spawn.z", 0),
                    config.getLong("spawn.yaw", 0),
                    config.getLong("spawn.pitch", 0));
        } else {
            SPAWN = Bukkit.getWorlds().get(0).getSpawnLocation();
        }
    }

    public static void setSpawn(JavaPlugin plugin, Location location) {
        FileConfiguration config = plugin.getConfig();
        config.set("spawn.world", location.getWorld().getName());
        config.set("spawn.x", location.getX());
        config.set("spawn.y", location.getY() + 0.5);
        config.set("spawn.z", location.getZ());
        config.set("spawn.yaw", location.getYaw());
        config.set("spawn.pitch", location.getPitch());
        plugin.saveConfig();
        SPAWN = location;
    }
}
