package net.pl3x.bukkit.spawn.commands;

import net.pl3x.bukkit.spawn.Pl3xSpawn;
import net.pl3x.bukkit.spawn.configuration.Config;
import net.pl3x.bukkit.spawn.configuration.Lang;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdSetSpawn implements TabExecutor {
    private final Pl3xSpawn plugin;

    public CmdSetSpawn(Pl3xSpawn plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.setspawn")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player player = (Player) sender;
        Location location = player.getLocation();

        location.getWorld().setSpawnLocation(
                location.getBlockX(),
                location.getBlockY(),
                location.getBlockZ());
        Config.setSpawn(plugin, location);

        Lang.send(sender, Lang.SET_SPAWN);
        return true;
    }
}
