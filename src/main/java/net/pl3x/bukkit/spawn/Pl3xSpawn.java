package net.pl3x.bukkit.spawn;

import net.pl3x.bukkit.spawn.commands.CmdPl3xSpawn;
import net.pl3x.bukkit.spawn.commands.CmdSetSpawn;
import net.pl3x.bukkit.spawn.commands.CmdSpawn;
import net.pl3x.bukkit.spawn.configuration.Config;
import net.pl3x.bukkit.spawn.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xSpawn extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        Bukkit.getPluginManager().registerEvents(this, this);

        getCommand("pl3xspawn").setExecutor(new CmdPl3xSpawn(this));
        getCommand("setspawn").setExecutor(new CmdSetSpawn(this));
        getCommand("spawn").setExecutor(new CmdSpawn(this));

        getLogger().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        getLogger().info(getName() + " disabled!");
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPlayedBefore()) {
            player.teleport(Config.SPAWN);
        }
    }
}
